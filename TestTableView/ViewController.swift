//
//  ViewController.swift
//  TestTableView
//
//  Created by Anton Kharchevskyi on 1/20/18.
//  Copyright © 2018 Anton Kharchevskyi. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var viewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableViewTopConstraint: NSLayoutConstraint!
  
    @IBOutlet weak var scrollHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var header: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    var offset: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        scrollView.bounces = false
//        tableView.bounces = false
        
        scrollView.delegate = self
        tableView.isScrollEnabled = false
        tableView.reloadData()
        
        
        scrollView.isScrollEnabled = true
        
        scrollHeightConstraint.constant = tableView.contentSize.height
        print(tableView.contentSize.height)
        
        let refresh = UIRefreshControl(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        tableView.refreshControl = refresh
    }
}


extension ViewController: UITableViewDataSource, UITabBarDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")!

        cell.backgroundColor = .blue
        cell.textLabel?.text = "\(indexPath.row)"
        return cell
    }
}

extension ViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let yOffset = scrollView.contentOffset.y

        if scrollView == self.scrollView {
            if yOffset >= scrollView.contentSize.height - scrollView.frame.height {
                scrollView.isScrollEnabled = false
                tableView.isScrollEnabled = true
            } else {
                tableViewTopConstraint.constant = 100
                tableView.refreshControl?.beginRefreshing()
            }
        }

        if scrollView == self.tableView {
            if yOffset <= 0 {
                self.scrollView.isScrollEnabled = true
                self.tableView.isScrollEnabled = false
            }
        }
    }
}
